# Author: Fabio Morea @ Area Science Park
# Package: Horizon-projects-network
# script: make network

# input:
# CORDIS dataset in two folders: H_20_csv and H_EU_csv

# output
# community detection by year

# SPDX-License-Identifier: CC-BY-4.0
# GitLab: https://gitlab.com/fabio-morea-areasciencepark/horizon-projects-network

# devtools::install_github('fabio-morea/CCD')

# libraries
options(warn = -1)
library(tidyverse)
library(lubridate)
library(readxl)
library(igraph)
library(CCD)
library(aricode)

# function
source("R_functions_network.r")
print(paste("Load data from file..."))
comms <- read_csv("communities.csv") %>%
  select(-comm_year_u) %>%
  mutate(CommY = paste0("temp", year, "_", comm_year_s)) %>%
  mutate(CommT = "-") %>%
  mutate(w = round(str / 1000, 1))


miny <- min(comms$year)
maxy <- max(comms$year)
print(paste("Analysing communities from", miny, "to", maxy))

print(paste("initialising CommT"))
year1 <- which(comms$year == miny)
# comms$CommT[year1] <- comms$CommY[year1]


evolution <- data.frame()


for (yy in (miny + 1):maxy) {
  print(paste("Processing year ", yy))
  last_year <- which(comms$year == (yy - 1))
  this_year <- which(comms$year == yy)
  com_last_year <- sort(unique(comms$CommY[last_year]))
  com_this_year <- sort(unique(comms$CommY[this_year]))

  for (i in 1:length(com_last_year)) {
    for (j in 1:length(com_this_year)) {
      # don't need to check intersection between comminity X
      if ((substr(com_last_year[i], 10, 10) == "X") & (substr(com_this_year[j], 10, 10) == "X")) {
        print("from X to X")
        next
      }
      if ((substr(com_last_year[i], 10, 10) == 0)) {
        print("from 0")
        # next
      }
      if ((substr(com_this_year[j], 10, 10) == 0)) {
        print("to 0")
        # next
      }

      print(paste(" --------------------------------------------- checking ", com_last_year[i], com_this_year[j]))
      label_i <- substr(com_last_year[i], 10, 13)
      label_j <- substr(com_this_year[j], 10, 13)
      ci <- comms %>% filter(comms$year == (yy - 1) & comms$comm_year_s == label_i) # questo test esclude la comunità X
      cj <- comms %>% filter(comms$year == (yy) & comms$comm_year_s == label_j) # questo test esclude la comunità X
      if (nrow(ci) * nrow(cj) > 0) {
        intersectionIDs <- intersect(ci$orgID, cj$orgID)
        x <- ci %>% filter(orgID %in% intersectionIDs)
        if ((nrow(x) > 0) & (sum(ci$w) > 0) & (sum(cj$w) > 0)) {
          xi <- round(sum(x$w) / sum(ci$w), 2) + 0
          xj <- round(sum(x$w) / sum(cj$w), 2) + 0
          print(paste("assessing intersection between", com_last_year[i], xi, "and", com_this_year[j], xj))

          if ((xi > 0.5) & (xj > 0.5)) {
            print(paste("SAME COMMUNITY over time!!", com_last_year[i], com_this_year[j]))
            evolution_type <- "Continue"
          } else if ((xi <= 0.5) & (xj <= 0.5)) {
            print(paste("remix: a small part of the community in year", yy - 1, "becomes a small part or another community in year ", yy))
            evolution_type <- "_shuffle"
          } else if ((xi > 0.5) & (xj <= 0.5)) {
            print(paste("MERGE: most of the community in year", yy - 1, "becomes a minor part or another community in year ", yy))
            evolution_type <- "Merge"
          } else if ((xi <= 0.5) & (xj > 0.5)) {
            print(paste("SPLIT: a small part of the community in year", yy - 1, "becomes the largest part or another community in year ", yy))
            evolution_type <- "Split"
          }
        } else {
          if (label_i == "X"){
            print(paste("from X", com_last_year[i], com_this_year[j]))
            evolution_type <- "fromX"
          } else if (label_j == "X"){
            print(paste("to X", com_last_year[i], com_this_year[j]))
            evolution_type <- "toX"
          } else {
            print("No intersection")
            next
          }


        }

        evolution <- rbind(
          evolution,
          data.frame(
            y1 = yy - 1,
            y2 = yy,
            # C1 = ,
            # C2 = ,
            CY1 = com_last_year[i],
            CY2 = com_this_year[j],
            type = evolution_type,
            w = sum(x$w)
          )
        )
      }
    }
  }
}

# evolution <- evolution %>% arrange(type)
print(evolution)
evolution <- evolution %>% mutate(w = round(w, 1))




# qui lavoriamo su EVOLUTION per assegnare le nuove etichette univoche CommT
# se vedo per la prima volta un'etichetta in C1, le do un numero
# l'etichetta in C2 è lo stesso numero se "CONTINUE", altrimenti è un nuovo numero

labels <- data.frame(temp = unique(c(evolution$CY1, evolution$CY2)))
labels$Com <- "-"



commTcounter <- 0
for (k in 1:nrow(evolution)) {
  selected_label_1 <- evolution$CY1[k]
  new_com_test_1 <- labels$Com[which(labels$temp == selected_label_1)]
  print(paste("Analysing row ", k, evolution$type[k], "first part", selected_label_1))
  if (new_com_test_1 == "-") {
    commTcounter <- commTcounter + 1
    labels$Com[which(labels$temp == selected_label_1)] <- commTcounter
    print(paste("the first community is new: assigning", commTcounter, "to ", selected_label_1))
  }
}
for (k in 1:nrow(evolution)) {
  selected_label_2 <- evolution$CY2[k]
  new_com_test_2 <- labels$Com[which(labels$temp == selected_label_2)]
  print(paste("analysing row ", k, evolution$type[k], "second part", selected_label_2))
  if (new_com_test_2 == "-") {
    commTcounter <- commTcounter + 1
    labels$Com[which(labels$temp == selected_label_2)] <- commTcounter
    print(paste("the second community is new: assigning", commTcounter, "to ", selected_label_2))
  }
}
evolution <- merge(evolution, labels, by.x = "CY1", by.y = "temp", all.x = TRUE) %>%
  rename(C_Y_1 = Com)
evolution <- merge(evolution, labels, by.x = "CY2", by.y = "temp", all.x = TRUE) %>%
  rename(C_Y_2 = Com)

# Continued communities : assign same label
for (k in 1:nrow(evolution)) {
  if (evolution$type[k] == "Continue"){
    #print("############")
    #print(evolution[k,])
    cont_label <- evolution$C_Y_1[k]
    to_be_replaced <- evolution$C_Y_2[k]
    #print(paste("Replacing ",cont_label ))
    evolution$C_Y_1[ evolution$C_Y_1 == to_be_replaced  ] <- cont_label
    evolution$C_Y_2[ evolution$C_Y_2 == to_be_replaced  ] <- cont_label
    #print(evolution[k,])


  } 
}

evolution$w[ evolution$type == "fromX"] <- 0.5
evolution$w[ evolution$type == "toX"] <- 0.5
evolution$C_Y_1[ evolution$type == "fromX"] <- "X"
evolution$C_Y_2[ evolution$type == "toX"] <- "X"

evolution %>%
  select(y1,CY1, C_Y_1,y2,CY2,C_Y_2,type,w)%>%
  write_csv("evolution.csv")

comms <- comms %>% mutate(CY = paste0("temp", year, "_", comm_year_s)) 
comms <- merge(comms, labels, by.x = "CY", by.y = "temp", all.x = TRUE) %>%   rename(C_Y = Com)
comms$C_Y[ comms$comm_year_s == "X" ] <- "X"
comms$C_Y[ comms$comm_year_s == "S" ] <- "S"
comms %>% write_csv("communities_newlabels.csv")
