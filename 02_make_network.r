# Author: Fabio Morea @ Area Science Park
# Package: Horizon-projects-network
# script: make network

# input: 
  # CORDIS dataset in two folders: H_20_csv and H_EU_csv 

# output
  # community detection by year

# SPDX-License-Identifier: CC-BY-4.0
# GitLab: https://gitlab.com/fabio-morea-areasciencepark/horizon-projects-network

#devtools::install_github('fabio-morea/CCD')

# libraries
options(warn=-1)
library(tidyverse)
library(lubridate)
library(readxl)
library(igraph)
library(CCD)
library(aricode)

# function
source('R_functions_network.r')

print(paste("Start reading files..."))
participation <- read_csv('./data/participation.csv', show_col_types = FALSE) %>%
    select(projID, orgID,  year, weight )  

orgs <- read_csv('./data/orgs.csv', show_col_types = FALSE)  %>%
  select(country,shortName,orgID,SME,activityType,nuts3,regione) %>%
  distinct(orgID, .keep_all = TRUE)

prjs <- read_csv('./data/project.csv', show_col_types = FALSE)  %>%
  select(projID, acronym, startDate, endDate) %>%
  rename(start_date = startDate, end_date = endDate)%>%
  distinct(projID, .keep_all = TRUE)

print(paste("loaded", nrow(orgs), "organisations and ", nrow(prjs), "projects"))
print(paste("participation is represented by ", nrow(participation), "edges"))
print(paste("Weight distribution from ", min(participation$weight), "to", max(participation$weight)))


# select a subset
# organisationID, name
# o999549887, AREA DI RICERCA SCIENTIFICA E TECNOLOGICA DI TRIESTE,AREA SCIENCE PARK 
# o999625644,ISTITUTO NAZIONALE DI OCEANOGRAFIA E DI GEOFISICA SPERIMENTALE,OGS, 
# o999850878,SCUOLA INTERNAZIONALE SUPERIORE DI STUDI AVANZATI DI TRIESTE,FALSE, 
# o996855421, comune di Trieste

selected_orgs <-c('o999549887','o999625644','o999850878', 'o996855421')
#selected_orgs <-c('o999625644')

print(paste("Filtering by selected organisations: ", (selected_orgs)))

selected_projects <-  participation %>% 
    filter(orgID %in% selected_orgs) %>% 
    select(projID) %>% 
    pull()

participation_filtered <- participation %>% 
  filter(projID %in% selected_projects) %>% 
  filter(weight > 0) 

print(paste("filtered", length(selected_orgs), "organisations and ", length(selected_projects), "projects"))
print(paste("participation is represented by ", nrow(participation_filtered), "edges"))
print(paste("Weight distribution from ", min(participation_filtered$weight), "to", max(participation_filtered$weight)))
x010<- quantile(participation_filtered$weight, 0.10)
#x095<-quantile(participation$weight, 0.95)
print(paste("Pruning small links at 10% confidence", x010))
participation_filtered <- participation_filtered %>% 
  filter(weight > x010) 
print(paste("Weight distribution from ", min(participation_filtered$weight), "to", max(participation_filtered$weight)))
print(paste("participation is represented by ", nrow(participation_filtered), "edges"))



#print(stophere)


# create bipartite network
#g_twomode  <- make_bipartite_network(participation_filtered, plot_network = TRUE)

## create organisations network
#print("### make a single giant network !")
#g_orgs <- make_orgs_network(participation_filtered, plot_network = FALSE)
#E(g_orgs)$weight <- E(g_orgs)$weight /1000

#print('## consensus')
#comms_consensus <- CCD::consensus_community_detection(g_orgs,method = "FG", t = 1,   p = 0.5, shuffle = TRUE)

#print(max(comms_consensus$membership))
#print(mean(comms_consensus$gamma))

#plot( comms_consensus , g_orgs, vertex.label = NA, vertex.color = as.factor(V(g_orgs)$community))
#V(g_orgs)$community <- comms_consensus$membership
#V(g_orgs)$gamma <- comms_consensus$gamma
#table(V(g_orgs)$gamma)

#df <- data.frame(k = coreness(g_orgs), s = strength(g_orgs), u = V(g_orgs)$gamma, com = as.factor(V(g_orgs)$community))
#windows();
#df %>% ggplot(aes(x=k, y=u)) + 
#geom_point(alpha = 0.5, aes(size=s*2, color = com))+theme_bw()

# create a network for every year
miny = min(participation_filtered$year)
maxy = max(participation_filtered$year)
#print(paste("analyse network by year from", miny, "to", maxy))

communities_by_year <- function(participation_filtered, year1, year2, t = 100, method = "LD", resolution = c(1.0)) {
results <- data.frame()
resolution = c(1.0)
  for (yy in (year1-1):(year2+1)) {
    print(paste("############### processing year", yy))
    results_year <- data.frame(orgID = unique(participation_filtered$orgID), year = yy, str = 0, comm_year_u = "X", gamma = 0)
    results_year$comm_year_u <- "X"
    results_year$gamma <- 0.0
    if ( (yy == year1-1) | (yy == year2+1) ){
      results_year$str <- 0
      results_year$comm_year_s <- "X" 
      results_year$community_size <- nrow(results_year)
      results <- rbind(results, results_year)
    } else {
      participation_year <- participation_filtered %>%
      filter(year == yy) %>%
      select(-year)
      print(paste("in year", yy, "there are ", nrow(participation_year), "edges"))
      g_orgs_y <- make_orgs_network(participation_year, network_name = as.character(yy), plot_network = F)
      V(g_orgs_y)$str <- strength(g_orgs_y)
    
      comms <- CCD::consensus_community_detection(g_orgs_y, 
        method = method, 
        resolution = resolution ,
        t = t, 
        p = 0.8, 
        shuffle = TRUE)
      V(g_orgs_y)$comm_y_u <- comms$membership
      V(g_orgs_y)$gamma <- round(comms$gamma, 1)


    # assign membership to results
      assigned_comms <- which(results_year$orgID %in% V(g_orgs_y)$name)
      results_year[assigned_comms, ]$comm_year_u <- V(g_orgs_y)$comm_y_u
      results_year[assigned_comms, ]$gamma <- V(g_orgs_y)$gamma

      results_year[assigned_comms, ]$str <- strength(g_orgs_y)
      # add a column community size and create a community "S" grouping all small communities
    
      # SMALL COMMUNITY threshold
      comm_S_threshold <- 15
      results_year <- results_year %>%
      group_by(comm_year_u) %>%
      mutate(community_size = n()) %>%
      ungroup() %>%
      mutate(comm_year_s = dense_rank(-community_size)-1) %>%
      mutate(comm_year_s = if_else(community_size <= comm_S_threshold, "S", as.character(comm_year_s))) %>%
      mutate(comm_year_s = if_else(comm_year_u == "X", "X", comm_year_s))

      results <- rbind(results, results_year)
    #s <- strength(g_orgs_y)
    #vsizenorm <- 10 * (s / max(s))^2
    # PLOT
    # plot(g_orgs_y, main = paste("anno", yy),
    #  vertex.size = vsizenorm,
    #  vertex.label = NA, layout = layout_with_fr)
    # print(table(comms$gamma))
      print(paste(yy, "number of orgs", vcount(g_orgs_y), "number of communities", max(comms$membership)))
      V(g_orgs_y)$community <- comms$membership
      E(g_orgs_y)$w <- E(g_orgs_y)$weight
      g_orgs_y<- delete.vertices(g_orgs_y, V(g_orgs_y)$comm_y_u == 'X')
      n2y <- CCD::make_community_network(g_orgs_y)
      plot(n2y,
        layout = layout.circle, vertex.size = V(n2y)$size,
        vertex.color = "blue",
        vertex.shape = "square", main = paste("communities in", yy)
      )
    }

  }
  return(results)
}

test1<- communities_by_year(participation_filtered, 2015, 2022, t = 10, resolution = c(1.0), method = "LD")
#test2<- communities_by_year(participation_filtered, 2018, 2019, t = 100, resolution = c(1.0), method = "LV")
#round(NMI(test1$comm_year, test2$comm_year),5)
#mean(test1$gamma)

communities <- test1 
communities%>%write_csv('communities.csv')

test1 %>%  
  filter(comm_year_s != 0)%>% # unassigned
  ggplot(aes(x = year, y = community_size, color = comm_year_s, group = comm_year_s)) + 
  geom_point()+ geom_line()
 

print(stophere)
