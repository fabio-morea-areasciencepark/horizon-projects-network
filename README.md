## Name
Horizon projects network

## Authors and acknowledgment
Author: Fabio Morea, Area Science Park, Trieste, Italy.

## Description
Horizon 2020 projects are a major initiative to foster collaborative research and innovation at European level. Results and metadata are available as open data on CORDIS portal; specifically the main dataset is https://cordis.europa.eu/data/cordis-h2020projects-csv.

The aim of this package is to 
1) improve **interoperability** of the dataset, by adding NUTS3 labels to any project
2) Facilitating the use of two **vocabularies** embedded in the dataset (namely: EuroSciVoc and calls) 
3) analyse **collaborations** between organisations (companies, academia, research centers...), by network analysis and community detection.  
## Project status
Development is ongoing as a part of my PhD program in Applied Data Science and Artificial Intelligence at University of Trieste. 

Scripts available to date: 
- **01_load_datasets**: download data from CORDIS, merge Horizon2020 and HorizonEurope. Extract EuroSciVoc and Topic. [coming soon: Geolocation: each organisation is labeled with a NUTS3 geo tag (trough its ZIP code). Improved geolocation for Italian organisation, managing city names and exceptions (e.g. "Naple"s is replaced with "Napoli")]. Coordinates are expanded as latitude and longitude; unconsistent data is removed. Results are stored in subfolder *data*.
  
- **s2_network_orgs**: building a weighted directed network where vertices are organisations and edges are project partnerships. Each project has a coordinator, which is connected to all other partners. The weight is proportional to project value (€). Several attributes are retained to allow multilayer anaoysis: call, EuroScivoc topic, year (start and end).
  
- **s3_network_countries**: the network is aggregated by country.
  
- **s4_network_regions**: the network is aggregated by region (NUTS3)
   

## Roadmap
comung up soon:
2) **geolocation_eu.py**: adding NUTS2 codes for all other European countries, based on coordinates and georeferenced polygons. 

3) **network.r**: creating a weighted directed network (nodes = organisations, edges = project collaborations, weight = EU contribution per partner). Project attributes include "euro-sci-voc" topics. Saving results as igraph, dataframe and html

4) **communities.r**: community detection and visualization

5) adding extra features (startups, spinoffs, balance sheet data)

 

## Usage
*Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.*

## Contributing
The project is open to contributions leading to open science publications. Contact the author at fabio.morea@areasciencepark.it. 

## License
SPDX-License-Identifier: CC-BY-4.0

