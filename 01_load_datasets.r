# Author: Fabio Morea @ Area Science Park
# Package: Horizon-projects-network
# script: data preparation

# input: 
  # download EUROSTAT/GISCO postcodes  (currently: only for Italy)
  # codes_call_1.xlsx

# actions:
  # download dataset from CORDIS repositories

# output: 
  # orgs.csv
  # projects.csv
  # project_call.csv  
  # project_topic_esv.csv
  ## topic_esv.csv
  ## project_topic_esv_table.csv
 
 

# SPDX-License-Identifier: CC-BY-4.0
# GitLab: https://gitlab.com/fabio-morea-areasciencepark/horizon-projects-network


 
# libraries
options(warn=-1)
library(tidyverse)
library(lubridate)
library(readxl)
library(igraph)

source('R_functions_data_prep.r')

# parameters: 
download_H2020 <- F
download_HEUROPE <- F

folder_HEurope <- "./data/H_EU_csv/"
folder_H2020 <- "./data/H_2020_csv/"

###### DOWNLOAD ##########
    # download datasets from: https://data.europa.eu/data/datasets
    ##  Horizon 2020 (2014-2020)
    ##  access page: https://data.europa.eu/data/datasets/cordish2020projects 
    ### download URL https://cordis.europa.eu/data/cordis-h2020projects-xlsx.zip

    ##  Horizon Europe (2021-2027)
    ##  access page: https://data.europa.eu/data/datasets/cordis-eu-research-projects-under-horizon-europe-2021-2027 
    ### download URL https://cordis.europa.eu/data/cordis-HORIZONprojects-xlsx.zip

if (download_H2020 == TRUE){
    download_dataset (
        cordis_address = "https://cordis.europa.eu/data/cordis-h2020projects-csv.zip", 
        local_folder = folder_H2020)
} 

if (download_HEUROPE == TRUE){
    download_dataset (
        cordis_address = "https://cordis.europa.eu/data/cordis-HORIZONprojects-xlsx.zip", 
        local_folder = folder_HEurope)
} 
# merge H2020 and HEUROPE data and save CSV files

## organisations: merge
print("Merging organisations...")
orgs <- rbind( read_orgs( folder_H2020 ), read_orgs( folder_HEurope) )  %>%
  mutate(orgID = paste0('o',organisationID)) %>% 
  mutate(projID = paste0('p', projectID)) %>%
  mutate(postCode = paste0(country, postCode)) %>%
  mutate(shortName = if_else( is.na(shortName), name, shortName )) %>% 
  distinct()
   
print("Adding NUTS3 geo-tags")
## organisations: add NUTS3 from zip codes from https://gisco-services.ec.europa.eu/tercet/flat-files https://gisco-services.ec.europa.eu/tercet/flat-files
# TODO download_dataset (cordis_address =  ..., local_folder = ...)
NUTS3zipcodes <- read_delim('./data_keys/nuts_zip_codes/pc2020_IT_NUTS-2021_v1.0.csv',
    show_col_types = FALSE,
    delim = ";",
    quote = "'",
    na = c("", "NA"),
		col_types = list(col_character())) %>%
    rename(postCode = CODE)%>%
    rename(nuts3 = NUTS3) %>%
    mutate(country = substr(nuts3,1,2)) %>%
    mutate(postCode = paste0(country, postCode))  %>%
    select(-country)

orgs <- merge(x = orgs, y = NUTS3zipcodes, by = 'postCode', all.x = TRUE)
#str(orgs)
# add italian region names
nuts23 <- read_delim('./data_keys/nuts_zip_codes/NUTS_IT.csv',
  delim = ";",
  show_col_types = FALSE) %>%
  select(nuts1,nuts2,nuts3, regione)

orgs <- merge(x = orgs, y = nuts23, by = 'nuts3', all.x = TRUE)
#str(orgs)
orgs_unique <- orgs %>%
  select(orgID,vatNumber,name,shortName,SME,activityType,country, nuts3, regione, city,geolocation, role) %>%
#  distinct(orgID, .keep_all = TRUE)%>%
  arrange(country, shortName,orgID)%>%
  relocate(country, shortName,orgID) 

orgs_unique %>% write_csv('./data/orgs.csv')

# projects
print("Merging projects...")

projects <- rbind( read_prjs( folder_H2020 ), read_prjs( folder_HEurope) ) %>%
  mutate(projID = paste0('p',projectID)) %>% 
  distinct(projID, .keep_all = TRUE) %>%
  filter(startDate > 0, endDate > 0, !is.na(startDate), !is.na(endDate)) %>%
  select(-projectID) 
  

str(projects)
calls <- rbind(read_legalbasis(folder_HEurope), read_legalbasis(folder_H2020)) %>%
  mutate(projID = paste0('p',projectID)) 
str(projects)
# participation table
print("Preparing participation table")

participation <- orgs %>%
	select("projID", "orgID","rcn","role", "ecContribution") %>% 
	mutate(coordinator = ifelse(role == "coordinator", TRUE, FALSE)) %>%
	select(-role)%>%
  filter(ecContribution > 10.0) %>%
	mutate(ecContribution = round(as.numeric(ecContribution)/1000,3))  
  


print("calculating number of partners per project")
n_partners <- participation %>% 
  group_by(projID) %>%
  summarise(n_partners = n())  
n_partners %>% write_csv('./data/npart.csv')

participation <- merge(participation, n_partners, by = "projID") %>%
    distinct(orgID, projID,  .keep_all = TRUE) 
    


## calculating weight by year
# Function to calculate the number of days of project activity in a given year
days_in_year <- function(startDate, endDate, year) {
  year_start <- as.Date(paste0(year, "-01-01"))
  year_end <- as.Date(paste0(year, "-12-31"))
 #print(paste(">>>", year_start, year_end, startDate, endDate))
  if (endDate < year_start || startDate > year_end || F) {
    return(0)
  } else {
    actual_start <- max(startDate, year_start)
    actual_end <- min(endDate, year_end)
    return(as.numeric(difftime(actual_end, actual_start, units = "days")) + 1)
    
  }
}
#days_in_year(as.Date('2015-02-01'),as.Date('2016-09-30'), 2015)
df <- left_join(participation, projects, by = 'projID') %>%
  filter(startDate > 0, endDate > 0, !is.na(startDate), !is.na(endDate))  


# Apply the function to each row and each year
for(year in 2014:2028) {
  print(paste("calculating weight by year...", year))
  df[[as.character(year)]] <- mapply(days_in_year, df$startDate, df$endDate, MoreArgs = list(year = year))
}
  
participation <- df %>%
  pivot_longer(cols = `2014`:`2028`, names_to = "year_weight", values_to = "days_active" ) %>%
  group_by(projID) %>%
  mutate(
    project_duration = as.numeric(difftime(endDate, startDate, units = "days")) + 1,
    normalized_by_project_duration = days_active / project_duration
  ) %>%
  ungroup() %>%
  mutate(weight = round(ecContribution * normalized_by_project_duration, 3)) %>%
  mutate(year = year_weight)%>%
  select(-days_active,-project_duration,-normalized_by_project_duration, -year_weight, -startDate, -endDate, -ecContribution)%>% 
  filter(weight > 0) 

participation %>% write_csv('./data/participation.csv')


print("preparing euroSciVoc codes")
print("Reading EuroSciVoc codes from local data folder")
filename_voc <- paste0(folder_HEurope, "euroSciVoc.csv") 
voc <- read_delim(filename_voc, delim = ";", quote = "\"", col_types = 'c') 

#extract euroSciVoc codes list
euroSciVoc <- voc %>% 
	select(euroSciVocPath, euroSciVocTitle,euroSciVocCode)%>%
	rename(description = euroSciVocTitle)%>%
	mutate(level1 = substring(euroSciVocPath,2,999))%>%
	mutate(level1 = sub("/.*", "", level1))%>%
	select(euroSciVocCode,level1, description)%>%
	arrange(level1)%>%
	distinct() 

euroSciVoc %>% 	arrange(euroSciVocCode) %>% write_csv("./data/topic_esv.csv")

tmp <- voc %>% select(projectID, euroSciVocCode)  %>%
  mutate(projID = paste0('p', projectID)) %>% select(-projectID)

tmp <- merge(x=projects, y=tmp, by = "projID",all.x=TRUE)

project_with_codes <- merge(x=tmp, y=euroSciVoc, by = "euroSciVocCode",all.x=TRUE) 

project_with_codes <- project_with_codes %>%
	select(projID, acronym, startDate, endDate, euroSciVocCode, level1 ) 

project_with_codes %>% write_csv("./data/project_topic_esv.csv")
print("completed the analysis of ESV topics")
print("note: some projects are NOT associated wit EuroSciVOc codes")

## TODO project_topic_table.csv
project_topic_table <- project_with_codes %>%
  select(projID,level1)%>%
  distinct()%>%
  mutate(value = 1)%>%
  pivot_wider( names_from = level1, values_from = value, values_fill = 0 ) 

colnames(project_topic_table) <- c("projID","nat_sci","soc_sci", "humani", "eng_tec", "med_heal", "agr_sci","not_spec")
project_topic_table <- project_topic_table %>%
   mutate(total = nat_sci + soc_sci + humani + eng_tec + med_heal + agr_sci + not_spec)%>%
   mutate(nat_sci  = if_else(nat_sci  > 0, nat_sci/total, 0.0)) %>%
   mutate(soc_sci  = if_else(soc_sci  > 0, soc_sci/total, 0.0)) %>%
   mutate(humani   = if_else(humani   > 0, humani/total, 0.0)) %>%
   mutate(eng_tec  = if_else(eng_tec  > 0, eng_tec/total, 0.0)) %>%
   mutate(med_heal = if_else(med_heal > 0, med_heal/total, 0.0)) %>%
   mutate(agr_sci = if_else(agr_sci > 0, agr_sci/total, 0.0)) 
   
colnames(project_topic_table)
project_topic_table %>% head()
project_topic_table %>% write_csv("./data/project_topic_esv_table.csv")


print("analysing call topics")

 
level_2_labels <- calls %>%
	select(group2,call_topic)%>%
	distinct(group2, .keep_all = TRUE)%>%
	arrange(group2)%>%
	write_csv("./data/project_codes_call_2.csv")	

level_1_labels <- read_excel('./data_keys/codes_call_1.xlsx')

calls <- merge(calls, level_1_labels, by="group")
calls %>%
	select(projID,group, description)%>%
	write_csv("./data/project_call.csv")

print("at this stage each project is associated with a level 1 label")
print(level_1_labels)
print("and a level 2 label")
print(level_2_labels)

print("Saving to projects file")
projects %>% write_csv("./data/project.csv")



print("Analysing EUROSCIVOC codes")
 


esv_tree <- voc %>%
  select(euroSciVocCode,euroSciVocPath)%>%
	distinct() %>%
  arrange(euroSciVocPath) %>%
  rowid_to_column( "index")


result <- tibble(level=0,esvpath="",dd="")
esv_tree$index
for (i in esv_tree$index){
  esvpath <- esv_tree$euroSciVocPath[i]
  separators = str_locate_all(pattern = "/",esvpath)[[1]]
  levels = length(separators[,1])
  breaks = c(unlist(separators[,1]),999999)
  if (i%%10==0){print(i)}
  for (j in 1:levels){
    desc <- substr(esvpath,breaks[j]+1,breaks[j+1]-1) 
    result <- result %>% add_row(
              level=j,
              esvpath=esv_tree$euroSciVocPath[i],
              dd=desc)
  }

}

result <- result %>%
  distinct() %>%
  arrange(level,dd)%>%
  relocate(level,dd) 

result %>%  write_csv('./data/esv_tree.csv')
  
 
print("Update completed ;-D")
 