#| Functions for data preparation

 
download_dataset <- function(cordis_address, local_folder){
    print("Downloading CORDIS data from cordis.europa.eu ")
    print("...download may take some minutes...")
    temp_dir <- "./tmp/"
    dir.create(temp_dir)
    raw_data_filename <- paste0(temp_dir,"rawdata.zip")
	print(raw_data_filename)
    download.file(cordis_address, raw_data_filename)
    unzip(raw_data_filename) # to current directory, creates 'csv' folder
    file.rename (from='./csv', to=local_folder) 
    file.remove(raw_data_filename)
    return(1)
}

read_orgs <- function(local_cordis_folder){
    filename <- paste0(local_cordis_folder, "organization.csv")
	print(paste("Reading ORGANISATIONS from", local_cordis_folder))
    data <- read_delim(filename, 
		delim = ";", 
		quote = "\"",
		na = c("", "NA"),
		col_types = list(col_character()))
	return(data)
}


read_prjs <- function(local_cordis_folder){
    filename <- paste0(local_cordis_folder, "project.csv")
	print(paste("Reading PROJECTS from", local_cordis_folder))
		data <- read_delim(filename,
		delim = ";",
		quote = "\"",
		col_types = list(col_character()))
	data <- data %>%
		mutate(projectID = id)%>%
		mutate(endYear = as.integer(year(endDate)))  %>%
		select(projectID, acronym, startDate, endDate, subCall, fundingScheme,legalBasis)
	return(data)
}

read_legalbasis <- function(local_cordis_folder){
	print(paste("Reading LEGAL BASIS from", local_cordis_folder))
	data <- read_delim(paste0(local_cordis_folder, "legalBasis.csv"), 
				delim = ";", 
				quote = "\"", 
				col_types = 'c') %>%
		mutate(len = str_length(legalBasis))%>%
		arrange(projectID, len, legalBasis)%>%
		select(projectID, title, legalBasis, len) %>%
		distinct(projectID, .keep_all = TRUE)%>%
		mutate(group = substring(legalBasis, 1,10) )%>%
		mutate(group2 = substring(legalBasis, 1,12) )%>%
		mutate(call_topic = title)%>%
		relocate(projectID,group)
	return(data)
}