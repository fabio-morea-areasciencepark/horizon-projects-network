# functions for network analysis

describe_g <-function(g, verbose = TRUE, plotg = TRUE){
  vc = vcount(g)
  ec = ecount(g)
  de = degree(g)
  kc = coreness(g)
  st = strength(g)
  dfg <- data.frame(de,st,kc) %>%
    arrange(-kc)
  #colnames(dfg) <- c('name', 'degree', 'strength', 'coreness')
  if (verbose){
    print(paste("Number of vertices: ", vc))  
    print(paste("Number of edges: ", ec))  
    print("Attributes:")
    print(list.vertex.attributes(g))
    print("gead of dataframe")
    print(head(dfg,10))
  }
  
  return(dfg)
}

make_bipartite_network <- function(participation, plot_network = FALSE){
  twomodeg <- graph_from_data_frame(participation, directed = FALSE)

  # set $type to manage bipartite network
  node_type <- substr(V(twomodeg)$name, 1,1)
  V(twomodeg)$node_type = node_type     # value is 'o' or 'p'
  V(twomodeg)$type = (node_type == 'o') # value is TRUE or FALSE
  
  if (plot_network){
      plot(twomodeg, 
      vertex.label = NA,
      vertex.size = 5,
      vertex.color = if_else(V(twomodeg)$node_type == 'o', 'green','red'),
      vertex.shape = if_else(V(twomodeg)$node_type == 'o', 'circle','square')
      )
  }
  return(twomodeg)
}
 

make_orgs_network <- function(participation, network_name = '', plot_network = FALSE){
  # input:  a long dataframe of edges (bimodal network): org, project weight
  # output: a long dataframe of edges (one-mode network): org1, org2, weight

  # B is the bi-adjacency matrix (orgs x projects)
  tmp <- participation %>% pivot_wider(
    names_from = projID, 
    values_from = weight, 
    values_fill = 0,
    values_fn = sum)

  B <- tmp %>% 
    select(-orgID) %>% 
    as.matrix()

  # projection as matrix product, distributes the weight among orgs
  P <- B %*% t(B)

  rownames(P) <- tmp$orgID
  colnames(P)<- tmp$orgID

  df_P <- data.frame(P) %>% 
    mutate(org1 = as.character(rownames(P))) %>% 
    pivot_longer( cols = -starts_with( c('org1', 'org2')), names_to = 'org2',  values_to = 'weight'  ) %>%
    filter(weight > 0.0)
  
  g_orgs <- igraph::simplify(graph_from_data_frame(df_P, directed = FALSE))
  delete.edges (g_orgs, which (E (g_orgs)$weight==0))

  if (plot_network){
    plot(g_orgs, main = network_name, 
        vertex.label = NA,
        vertex.size = 1,
        vertex.color = "green",
        vertex.shape = "circle")
  }
  
  return(g_orgs)
}